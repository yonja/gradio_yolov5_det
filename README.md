<p align="center">
<a href="https://gitee.com/CV_Lab/gradio_yolov5_det">
<img src="https://pycver.gitee.io/ows-pics/imgs/gradio_yolov5_det_logo.png" alt="Simple Icons" >
</a>
<p align="center">
    基于Gradio的YOLOv5通用目标检测演示系统
</p>
<p align="center">
    可自定义检测模型、演示便捷、安装简单
</p>
</p>
<p align="center">
<a href="./CodeCheck.md"><img src="https://img.shields.io/badge/CodeCheck-passing-success" alt="code check" /></a>
<a href="https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.5"><img src="https://img.shields.io/badge/Releases-v0.5-green" alt="Releases Version" /></a>
<a href="https://github.com/gradio-app/awesome-demos#-computer-vision"><img src="https://img.shields.io/badge/Gradio-awesome--demos-brightgreen" alt="gradio awesome demos" /></a>
<a href="https://huggingface.co/"><img src="https://img.shields.io/badge/%F0%9F%A4%97-Hugging%20Face-blue" alt="Hugging Face Spaces" /></a>
<a href="https://huggingface.co/spaces"><img src="https://img.shields.io/badge/🤗%20Hugging%20Face-Spaces-blue" alt="Hugging Face Spaces" /></a>
<a href="https://gitee.com/CV_Lab/gradio_yolov5_det/blob/master/LICENSE"><img src="https://img.shields.io/badge/License-GPL--3.0-blue" alt="License" /></a>
</p>
<p align="center">
<a href="https://github.com/ultralytics/yolov5"><img src="https://img.shields.io/badge/YOLOv5-v6.2-blue" alt="YOLOv5 Version" /></a>
<a href="https://github.com/gradio-app/gradio"><img src="https://img.shields.io/badge/Gradio-3.3.1+-orange" alt="Gradio Version" /></a>
<a href="#"><img src="https://img.shields.io/badge/Python-3.8%2B-blue?logo=python" alt="Python Version" /></a>
<a href="https://pypi.org/project/torch/"><img src="https://img.shields.io/badge/torch-1.12.0%2B-important?logo=pytorch" alt="Torch Version" /></a>
<a href="https://pypi.org/project/torchvision/"><img src="https://img.shields.io/badge/torchvision-0.13.0%2B-green?logo=pytorch" alt="TorchVision Version" /></a>
<a href="https://github.com/pre-commit/pre-commit"><img src="https://img.shields.io/badge/checks-pre--commit-brightgreen" alt="pre-commit"></a>
</p>
<p align="center">
<a href='https://gitee.com/CV_Lab/gradio_yolov5_det/stargazers'><img src='https://gitee.com/CV_Lab/gradio_yolov5_det/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/CV_Lab/gradio_yolov5_det/members'><img src='https://gitee.com/CV_Lab/gradio_yolov5_det/badge/fork.svg?theme=dark' alt='fork'></img></a>
</p>


## 🚀 作者简介

曾逸夫，从事人工智能研究与开发；主研领域：计算机视觉；[YOLOv5官方开源项目代码贡献人](https://github.com/ultralytics/yolov5/graphs/contributors)；[YOLOv5 v6.1代码贡献人](https://github.com/ultralytics/yolov5/releases/tag/v6.1)；[YOLOv5 v6.2代码贡献人](https://github.com/ultralytics/yolov5/releases/tag/v6.2)；[Gradio官方开源项目代码贡献人](https://github.com/gradio-app/gradio/graphs/contributors)

❤️  Github：https://github.com/Zengyf-CVer

🔥 YOLOv5 官方开源项目PR ID：

- Save \*.npy features on detect.py `--visualize`：https://github.com/ultralytics/yolov5/pull/5701
- Fix `detect.py --view-img` for non-ASCII paths：https://github.com/ultralytics/yolov5/pull/7093
- Fix Flask REST API：https://github.com/ultralytics/yolov5/pull/7210
- Add yesqa to precommit checks：https://github.com/ultralytics/yolov5/pull/7511
- Add mdformat to precommit checks and update other version：https://github.com/ultralytics/yolov5/pull/7529
- Add TensorRT dependencies：https://github.com/ultralytics/yolov5/pull/8553

💡 YOLOv5 v6.1 & v6.2代码贡献链接：

- https://github.com/ultralytics/yolov5/releases/tag/v6.1
- https://github.com/ultralytics/yolov5/releases/tag/v6.2

🔥 Gradio 官方开源项目PR ID：

- Create a color generator demo：https://github.com/gradio-app/gradio/pull/1872

<h2 align="center">🚀更新走势</h2>

- `2022-08-04` **⚡ [Gradio YOLOv5 Det v0.5](https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.5)正式上线**
- `2022-05-31` **⚡ [Gradio YOLOv5 Det v0.4](https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.4)正式上线**
- `2022-05-22` **⚡ [Gradio YOLOv5 Det v0.3](https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.3)正式上线**
- `2022-05-14` **🚀 \[好消息\] [Gradio YOLOv5 Det v0.3](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v3)** 正式加入到 **[Gradio官方demo列表](https://github.com/gradio-app/awesome-demos)** 🎉🎉🎉🎉🎉
- `2022-05-13` **🚀\[推荐\] [Gradio YOLOv5 Det 开发版 脚本指令操作](https://gitee.com/CV_Lab/gradio_yolov5_det#-%E8%84%9A%E6%9C%AC%E6%8C%87%E4%BB%A4%E6%93%8D%E4%BD%9C-%E6%8E%A8%E8%8D%90)**
- `2022-05-12` **⚡ [Gradio YOLOv5 Det v0.2.2](https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.2.2)正式上线**
- `2022-05-08` **⚡ [Gradio YOLOv5 Det v0.2](https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.2)正式上线**
- `2022-04-30` **⚡ [Gradio YOLOv5 Det v0.1](https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.1)正式上线**
- ⚡ [Gradio YOLOv5 Det 历史版本README](./history_version)

<h2 align="center">🤗在线Demo</h2>

### **🚀 [Gradio YOLOv5 Det v0.3](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v3)** 正式加入到[Gradio官方demo列表](https://github.com/gradio-app/awesome-demos) 🤗

❤️ 点击下图进入[Gradio awesome-demos](https://github.com/gradio-app/awesome-demos) ，在**Computer vision**列表中***Find Me!***

<div align="center" >
<a href="https://github.com/gradio-app/awesome-demos#-computer-vision">
<img src="https://pycver.gitee.io/ows-pics/imgs/gradio_awesome_demos.png">
</a>
</div>

### ❤️ 快速体验

本项目提供了**在线demo**，点击下面的logo，进入**Hugging Face Spaces**中快速体验：

<div align="center" >
<a href="https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v5">
<img src="https://pycver.gitee.io/ows-pics/imgs/huggingface_logo.png">
</a>
</div>


### 💡 Demo 列表

❤️ 点击列表中的链接，进入对应版本的**Hugging Face Spaces**界面中快速体验：

|                          Demo 名称                           |                       整体界面-检测前                        |                       整体界面-检测后                        |                             状态                             |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| 🚀 [Gradio YOLOv5 Det v0.5](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v5) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v05_img_screenshot01.png) ,[Video](https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_screenshot01.png) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v05_img_screenshot02.png) ,[Video](https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_screenshot02.png) | [![demo status](https://img.shields.io/website-up-down-green-red/https/hf.space/gradioiframe/Zengyf-CVer/Gradio_YOLOv5_Det_v5/+.svg?label=demo%20status)](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v5) |
| 🚀 [Gradio YOLOv5 Det v0.4](https://huggingface.co/spaces/Gradio-Blocks/Gradio_YOLOv5_Det) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v4_screenshot.png) ,[Video](https://pycver.gitee.io/ows-pics/imgs/gyd_v4_screenshot02.png) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v4_screenshot03.png) | [![demo status](https://img.shields.io/website-up-down-green-red/https/hf.space/gradioiframe/Zengyf-CVer/Gradio_YOLOv5_Det_v4/+.svg?label=demo%20status)](https://huggingface.co/spaces/Gradio-Blocks/Gradio_YOLOv5_Det) |
| [Gradio YOLOv5 Det v0.3](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v3) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v3_screenshot.png) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v3_screenshot02.png) | [![demo status](https://img.shields.io/website-up-down-green-red/https/hf.space/gradioiframe/Zengyf-CVer/Gradio_YOLOv5_Det_v3/+.svg?label=demo%20status)](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v3) |
| [Gradio YOLOv5 Det v0.2.2](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v2_2) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v2_2_screenshot.png) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v2_2_screenshot02.png) | [![demo status](https://img.shields.io/website-up-down-green-red/https/hf.space/gradioiframe/Zengyf-CVer/Gradio_YOLOv5_Det_v2_2/+.svg?label=demo%20status)](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v2_2) |
| [Gradio YOLOv5 Det v0.2](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v2) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v2_screenshot.png) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v1_screenshot02.png) | [![demo status](https://img.shields.io/website-up-down-green-red/https/hf.space/gradioiframe/Zengyf-CVer/Gradio_YOLOv5_Det_v2/+.svg?label=demo%20status)](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v2) |
| [Gradio YOLOv5 Det v0.1](https://huggingface.co/spaces/Zengyf-CVer/gradio_yolov5_det) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v1_screenshot.png) | [Image](https://pycver.gitee.io/ows-pics/imgs/gyd_v1_screenshot02.png) | [![demo status](https://img.shields.io/website-up-down-green-red/https/hf.space/gradioiframe/Zengyf-CVer/gradio_yolov5_det/+.svg?label=demo%20status)](https://huggingface.co/spaces/Zengyf-CVer/gradio_yolov5_det) |

❗ 注：点击`整体界面`链接，查看项目**整体界面大图**

|                          Demo 名称                           | 输入类型  |             输出类型             |
| :----------------------------------------------------------: | :-------: | :------------------------------: |
| 🚀 [Gradio YOLOv5 Det v0.5](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v5) | 图片/视频 | 图片/视频/JSON/PDF/数据表/统计图 |
| 🚀 [Gradio YOLOv5 Det v0.4](https://huggingface.co/spaces/Gradio-Blocks/Gradio_YOLOv5_Det) | 图片/视频 |    图片/视频/JSON/PDF/数据表     |
| [Gradio YOLOv5 Det v0.3](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v3) |   图片    |       图片/JSON/PDF/数据表       |
| [Gradio YOLOv5 Det v0.2.2](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v2_2) |   图片    |          图片/JSON/PDF           |
| [Gradio YOLOv5 Det v0.2](https://huggingface.co/spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v2) |   图片    |          图片/JSON/PDF           |
| [Gradio YOLOv5 Det v0.1](https://huggingface.co/spaces/Zengyf-CVer/gradio_yolov5_det) |   图片    |            图片/JSON             |

### ⚡ 自定义在线Demo

#### ❤️ 方法一：gradio.Interface.load()

点击下图，查看详细代码：

<div align="center" >
<a href="./huggingface_demo">
<img src="https://pycver.gitee.io/ows-pics/imgs/huggingface_demo.png">
</a>
</div>

#### ❤️ 方法二：gradio.js

点击下图，查看详细代码：

<div align="center" >
<a href="./gradiojs">
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_gradiojs.png">
</a>
</div>

#### ❤️ 方法三：gradio iframe

查看[gradio iframe](./gradio_iframe/gradio_iframe.md)详细代码：

```html
<iframe src="https://hf.space/embed/Gradio-Blocks/Gradio_YOLOv5_Det/+" frameBorder="0" width="1300px" height="1000px" title="Gradio app" allow="accelerometer; ambient-light-sensor; autoplay; battery; camera; document-domain; encrypted-media; fullscreen; geolocation; gyroscope; layout-animations; legacy-image-formats; magnetometer; microphone; midi; oversized-images; payment; picture-in-picture; publickey-credentials-get; sync-xhr; usb; vr ; wake-lock; xr-spatial-tracking" sandbox="allow-forms allow-modals allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-downloads"></iframe>
```

<h2 align="center">💎项目流程与用途</h2>

### 📌 项目整体流程

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v04_workflow.png">
</div>

### 📌 项目示例

#### ❤️ Gradio YOLOv5 Det v0.5 界面与检测效果（图片模式）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_01.png">
</div>

#### ❤️ 输入界面（图片模式）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_02.png">
</div>

<div align="center" >
输入界面01
</div>
<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_03.png">
</div>

<div align="center" >
输入界面02
</div>

#### ❤️ 输出界面（图片模式）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_04.png">
</div>

<div align="center" >
输出界面01
</div>

<div align="center">
        <img src="https://pycver.gitee.io/ows-pics/imgs/object_crop01.png" width="45%"/>
        <img src="https://pycver.gitee.io/ows-pics/imgs/object_crop02.png" width="45%"/>
</div>

<div align="center" >
输出界面02（v0.4 实例查看）
</div>
<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_05.png">
</div>
<div align="center" >
输出界面03
</div>

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_06.png">
</div>

<div align="center" >
输出界面04
</div>

#### ❤️ 快速体验（图片模式）

本项目提供了4个**图片示例**，用户可以快速体验检测效果：

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v03_examples.png">
</div>
<div align="center" >
示例界面
</div>

#### ❤️ Gradio YOLOv5 Det v0.5 界面与检测效果（视频模式）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_01.png">
</div>

#### ❤️ 输入界面（视频模式）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_02.png">
</div>

<div align="center" >
输入界面01
</div>
<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_03.png">
</div>

<div align="center" >
输入界面02
</div>

#### ❤️ 输出界面（视频模式）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_04.png">
</div>

<div align="center" >
输出界面01
</div>

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_05.png">
</div>
<div align="center" >
输出界面02
</div>

#### ❤️ 快速体验（视频模式）

本项目提供了3个**视频示例**，用户可以快速体验检测效果：

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v05_video_06.png">
</div>
<div align="center" >
示例界面
</div>

<h2 align="center">💡项目结构</h2>

```
.
├── gradio_yolov5_det						# 项目名称
│   ├── model_download						# 模型下载
│   │   ├── yolov5_model_p5_p6_all.sh		# YOLOv5 P5模型
│   │   ├── yolov5_model_p5_all.sh			# YOLOv5 P5模型
│   │   ├── yolov5_model_p6_all.sh			# YOLOv5 P6模型
│   │   └── yolov5_model_p5_n.sh			# yolov5n模型
│   ├── model_config						# 模型配置
│   │   ├── model_name_p5_all.yaml			# YOLOv5 P5 模型名称（yaml版）
│   │   ├── model_name_p6_all.yaml			# YOLOv5 P6 模型名称（yaml版）
│   │   ├── model_name_p5_p6_all.yaml		# YOLOv5 P5 & P6 模型名称（yaml版）
│   │   ├── model_name_p5_n.yaml			# yolov5n 模型名称（yaml版）
│   │   ├── model_name_p5_all.csv			# YOLOv5 P5 模型名称（csv版）
│   │   ├── model_name_p6_all.csv			# YOLOv5 P6 模型名称（csv版）
│   │   └── model_name_p5_n.csv				# yolov5n 模型名称（csv版）
│   ├── cls_name							# 类别名称
│   │   ├── cls_name_zh.yaml				# 类别名称文件（yaml版-中文）
│   │   ├── cls_name_en.yaml				# 类别名称文件（yaml版-英文）
│   │   ├── cls_name_ru.yaml				# 类别名称文件（yaml版-俄语）
│   │   ├── cls_name_es.yaml				# 类别名称文件（yaml版-西班牙语）
│   │   ├── cls_name_ar.yaml				# 类别名称文件（yaml版-阿拉伯语）
│   │   ├── cls_name_ko.yaml				# 类别名称文件（yaml版-韩语）
│   │   ├── cls_name.yaml					# 类别名称文件（yaml版-中文-v0.1）
│   │   └── cls_name.csv					# 类别名称文件（csv版-中文）
│   ├── huggingface_demo					# 自定义在线HuggingFace Demo
│   │   ├── gyd_hf_demo_v4.py				# Gradio YOLOv5 Det v0.4 脚本
│   │   └── gyd_hf_demo_v3.py				# Gradio YOLOv5 Det v0.3 脚本
│   ├── models								# 模型Hub
│   │   ├── readme.md						# 模型Hub README
│   │   ├── *.pt							# PyTorch模型
│   │   └── *.onnx							# ONNX模型
│   ├── util								# 工具包
│   │   ├── fonts_opt.py					# 字体管理
│   │   └── pdf_opt.py						# PDF管理
│   ├── history_version						# 历史版本README
│   │   ├── v1_v2.md						# v1_v2 中文README
│   │   └── v1_v2.en.md						# v1_v2 英文README
│   ├── img_examples						# 示例图片
│   ├── __init__.py							# 初始化文件
│   ├── gradio_yolov5_det_v5.py				# v0.5主运行文件
│   ├── gradio_yolov5_det_v5_dev.py			# v0.5主运行文件（热重载版）
│   ├── gradio_yolov5_det_v4.py				# v0.4主运行文件
│   ├── gradio_yolov5_det_v3.py				# v0.3主运行文件
│   ├── gradio_yolov5_det_v2_2.py			# v0.2.2主运行文件
│   ├── gradio_yolov5_det_v2.py				# v0.2主运行文件
│   ├── gradio_yolov5_det.py				# v0.1主运行文件
│   ├── LICENSE								# 项目许可
│   ├── CodeCheck.md						# 代码检查
│   ├── .gitignore							# git忽略文件
│   ├── yolov5_pytorch_gpu.md				# YOLOv5 PyTorch GPU安装教程
│   ├── README.md							# 项目说明
│   ├── README.en.md						# 项目说明（英文版）
│   └── requirements.txt					# 脚本依赖包
```

<h2 align="center">❤️ 版本改进</h2>

🔥 [Gradio YOLOv5 Det v0.4](./gradio_yolov5_det_v4.py) 采用**标签和边界框颜色模式**：（点击图片，可查看大图）

|                                     v0.3                                      |                                       v0.4                                       |
| :---------------------------------------------------------------------------: | :------------------------------------------------------------------------------: |
| <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v02_zh.png" width="45%"/> | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v04_color.png" width="45%"/> |

🔥 [Gradio-YOLOv5-Det v0.2](./gradio_yolov5_det_v2.py) 采用[Pillow](https://github.com/python-pillow/Pillow)组件绘制图片检测效果，可**自定义字体**文件。

<p align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/opencv_pillow.png">
</p>

🔥 [Gradio-YOLOv5-Det v0.2](./gradio_yolov5_det_v2.py) 的检测结果可以在图片上**显示ID、自定义字体标签以及置信度**，同时提供了**6**种语言：**中文、英文、俄语、西班牙语、阿拉伯语以及韩语**，具体效果如下图所示。（点击图片，可查看大图）

|     版本-语言     |                                      效果图                                      |
| :-----------: | :---------------------------------------------------------------------------: |
|    v0.1-英文    | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v01_en.png" width="45%"/> |
|  **v0.2-中文**  | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v02_zh.png" width="45%"/> |
|  **v0.2-英文**  | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v02_en.png" width="45%"/> |
|  **v0.2-俄语**  | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v02_ru.png" width="45%"/> |
| **v0.2-西班牙语** | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v02_es.png" width="45%"/> |
| **v0.2-阿拉伯语** | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v02_ar.png" width="45%"/> |
|  **v0.2-韩语**  | <img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v02_ko.png" width="45%"/> |

<h2 align="center">🔥安装教程</h2>

### ✅ 第一步：创建conda环境

```shell
conda create -n yolo python==3.8
conda activate yolo # 进入环境
```

### ✅ 第二步：克隆

```shell
git clone https://gitee.com/CV_Lab/gradio_yolov5_det.git
```

### ✅ 第三步：安装Gradio YOLOv5 Det依赖

```shell
cd gradio_yolov5_det
pip install -r ./requirements.txt -U
```

❗ 注意：yolov5默认采用pip安装PyTorch GPU版，如果采用官网安装**PyTorch GPU**版，参见[YOLOv5 PyTorch GPU安装教程](./yolov5_pytorch_gpu.md)

<h2 align="center">⚡使用教程</h2>

❤️ 注：**Gradio YOLOv5 Det v0.5.x** 的使用方法和v0.1、v0.2.x、v0.3.x、v0.4.x相同，将`gradio_yolov5_det.py` 改为 `gradio_yolov5_det_v5_x.py` 即可。

### 💡 运行YOLOv5模型

📌 下载YOLOv5 P5模型

❤️ yolov5n.pt--yolov5x.pt，yolov5n6.pt--yolov5x6.pt下载到`models`目录中

```shell
bash ./model_download/yolov5_model_p5_p6_all.sh
```

📌 运行

```shell
python gradio_yolov5_det_v5.py # v0.5
python gradio_yolov5_det_v4.py # v0.4
python gradio_yolov5_det_v3.py # v0.3
# 在浏览器中输入：http://127.0.0.1:7860/或者http://127.0.0.1:7861/ 等等（具体观察shell提示）
```

❗ 注意：v0.1和v0.2.x版本运行如下：

```shell
cd gradio_yolov5_det && git clone https://github.com/ultralytics/yolov5.git
cp *.pt yolov5
python gradio_yolov5_det.py # v0.1
python gradio_yolov5_det_v2.py # v0.2
python gradio_yolov5_det_v2_2.py # v0.2.2
```

❤️ 本项目提供了4种YOLOv5模型下载脚本，默认为`yolov5_model_p5_p6_all.sh`

```shell
# yolov5n模型下载及运行
bash ./model_download/yolov5_model_p5_n.sh
python gradio_yolov5_det.py -mc ./model_config/model_name_p5_n.yaml （yaml版）
# python gradio_yolov5_det.py -mc ./model_config/model_name_p5_n.csv （csv版）

# YOLOv5 P5模型下载及运行
bash ./model_download/yolov5_model_p5_all.sh
python gradio_yolov5_det.py -mc ./model_config/model_name_p5_all.yaml （yaml版）
# python gradio_yolov5_det.py -mc ./model_config/model_name_p5_all.csv （csv版）

# YOLOv5 P6模型下载及运行
bash ./model_download/yolov5_model_p6_all.sh
python gradio_yolov5_det.py -mc ./model_config/model_name_p6_all.yaml （yaml版）
# python gradio_yolov5_det.py -mc ./model_config/model_name_p6_all.csv （csv版）
```

❗ 注：默认类别文件[cls_name_zh.yaml](./cls_name/cls_name_zh.yaml)|[cls_name.csv](./cls_name/cls_name.csv)

### 💡 运行自定义模型

📌 自定义模型

❤️ 将自定义模型文件（\*.pt）放入到`models` 目录中

```shell
python gradio_yolov5_det.py -mc custom_model_name.yaml -cls custom_model_cls_name.yaml （yaml版）
# python gradio_yolov5_det.py -mc custom_model_name.csv -cls custom_model_cls_name.csv （csv版）

# 在浏览器中输入：http://127.0.0.1:7860/或者http://127.0.0.1:7861/ 等等（具体观察shell提示）
```

📌 自定义模型名称

`custom_model_name.yaml`格式：

```yaml
model_names: ["widerface-s", "widerface-m", "widerface-l"]
```

`custom_model_name.csv`格式（`\n`分隔）：

```shell
widerface-s
widerface-m
widerface-l
```

📌 自定义模型类别

`custom_model_cls_name.yaml`格式：

```yaml
model_cls_name: ["face"]
```

`custom_model_cls_name.csv`格式（`\n`分隔）：

```shell
face
```

### 💡 脚本指令操作 \[推荐\]

❤️ 本项目提供了一些脚本指令，旨在扩展项目的功能。

❗ 注：其中的一些功能是界面组件（按钮、文本框等）无法实现的，需要通过脚本指令完成：

```shell
# 登录模式（v0.3）
python gradio_yolov5_det.py -isl # 默认用户名\密码：admin, admin
python gradio_yolov5_det.py -isl -up zyf zyf # 设置用户名\密码：zyf, zyf

# 共享模式（v0.3）
python gradio_yolov5_det.py -is # 在浏览器中以共享模式打开，https://**.gradio.app/

# 自定义端口号
python gradio_yolov5_det.py -sp 8080 #（v0.6）

# 图片输入源切换，默认为图片上传（v0.3）
python gradio_yolov5_det.py -src upload # 图片上传
python gradio_yolov5_det.py -src webcam # webcam拍照

# 视频输入源切换，默认为视频上传（v0.4）
python gradio_yolov5_det.py -src_v upload # 视频上传
python gradio_yolov5_det.py -src_v webcam # webcam录制

# 输入图片操作模式，默认为图片编辑器（v0.3）
python gradio_yolov5_det.py -it editor # 图片编辑器
python gradio_yolov5_det.py -it select # 区域选择

# 设备切换（cuda 或者 cpu）
python gradio_yolov5_det.py -dev cuda:0 # cuda
python gradio_yolov5_det.py -dev cpu # cpu

# 自定义下拉框默认模型名称
python gradio_yolov5_det.py -mn yolov5m

# 自定义NMS置信度阈值
python gradio_yolov5_det.py -conf 0.8

# 自定义NMS IoU阈值
python gradio_yolov5_det.py -iou 0.5

# 设置推理尺寸，默认为640
python gradio_yolov5_det.py -isz 320

# 设置最大检测数，默认为50（v0.3）
python gradio_yolov5_det.py -mdn 100

# 设置滑块步长，默认为0.05（v0.3）
python gradio_yolov5_det.py -ss 0.01

# 设置默认不显示检测标签，注：v0.3弃用
python gradio_yolov5_det.py -lds
```

### 💡 热重载（开发模式）

**Gradio YOLOv5 Det v0.5 开发版** 提供了**热重载**模式，可以实时调试程序：

```shell
gradio gradio_yolov5_det_v5_dev.py gyd
```

❗ 注：**gradio>=3.0.17**

### 💡 其他功能

`Det_Report.pdf`内容如下：

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/gyd_v03_pdf.png">
</div>

相关检测数据存储在`run`目录中，结构如下：

```
# run目录
.
├── run						# 人脸数据保存目录
│   ├── 原始图片				# 原始图片目录
│   │   ├── 0.jpg			# 原始图片
│   │   ├──	1.jpg			# 原始图片
│   │   ├──	......
│   ├── 检测图片				# 检测图片目录
│   │   ├── 0.png			# 检测图片
│   │   ├── 1.png			# 检测图片
│   │   ├──	......
│   ├── 下载检测报告			# 检测报告目录
│   │   ├── 0.pdf			# 检测图片
│   │   ├── 1.pdf			# 检测图片
│   │   ├──	......
│   ├── log.csv				# 检测日志
```

### 📝 项目引用指南

📌 如需引用Gradio YOLOv5 Det v0.5，请在相关文章的**参考文献**中加入下面文字：

```
曾逸夫, (2022) Gradio YOLOv5 Det (Version 0.5).https://gitee.com/CV_Lab/gradio_yolov5_det.
```

📌 如需引用Gradio YOLOv5 Det v0.4，请在相关文章的**参考文献**中加入下面文字：

```
曾逸夫, (2022) Gradio YOLOv5 Det (Version 0.4).https://gitee.com/CV_Lab/gradio_yolov5_det.
```

📌 如需引用Gradio YOLOv5 Det v0.3，请在相关文章的**参考文献**中加入下面文字：

```
曾逸夫, (2022) Gradio YOLOv5 Det (Version 0.3).https://gitee.com/CV_Lab/gradio_yolov5_det.
```

### 💬 技术交流

- 如果你发现任何Gradio YOLOv5 Det存在的问题或者是建议, 欢迎通过[Gitee Issues](https://gitee.com/CV_Lab/gradio_yolov5_det/issues)给我提issues。
- 欢迎加入CV Lab技术交流群

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/qq_group.jpg" width="20%">
</div>
