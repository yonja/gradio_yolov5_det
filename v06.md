# Gradio YOLOv5 Det v0.6

## 创建人：曾逸夫

### 💡 功能更新

- 加入PDF图片嵌入

### 🔧 BUG修复

- 修复模型加载的不稳定性
