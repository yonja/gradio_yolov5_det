# gradio iframe

## 创建人：曾逸夫

### Gradio YOLOv5 Det v0.4（iframe版）

❗ 注：本代码在可识别**iframe**标签的软件中可用，比如Typora：

<iframe src="https://hf.space/embed/Gradio-Blocks/Gradio_YOLOv5_Det/+" frameBorder="0" width="1300px" height="1000px" title="Gradio app" allow="accelerometer; ambient-light-sensor; autoplay; battery; camera; document-domain; encrypted-media; fullscreen; geolocation; gyroscope; layout-animations; legacy-image-formats; magnetometer; microphone; midi; oversized-images; payment; picture-in-picture; publickey-credentials-get; sync-xhr; usb; vr ; wake-lock; xr-spatial-tracking" sandbox="allow-forms allow-modals allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-downloads"></iframe>
